#!/bin/bash
sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin &&
sudo dnf groupupdate sound-and-video &&
sudo dnf install nvidia-vaapi-driver &&
sudo dnf install rpmfusion-nonfree-release-tainted &&
sudo dnf --repo=rpmfusion-nonfree-tainted install "*-firmware"
