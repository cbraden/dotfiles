### Flatpak Icons in Silverblue/Ublue

```
sudo gtk-update-icon-cache -f /var/lib/flatpak/exports/share/icons/hicolor/ 
sudo gtk4-update-icon-cache -f /var/lib/flatpak/exports/share/icons/hicolor/
```

### Disable SELinux

1. Edit SELinux config:

```
sudo nvim /etc/selinux/config
```
2. Configure the SELINUX=disabled option:

```
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#       enforcing - SELinux security policy is enforced.
#       permissive - SELinux prints warnings instead of enforcing.
#       disabled - No SELinux policy is loaded.
SELINUX=disabled
# SELINUXTYPE= can take one of these two values:
#       targeted - Targeted processes are protected,
#       mls - Multi Level Security protection.
SELINUXTYPE=targeted

```
3. Reboot the system.

---

### GNOME check alive timeout

Change GNOME check alive timeout:

```
gsettings set org.gnome.mutter check-alive-timeout 80000
```

---

### Have GDM use same refresh rate as login session.

```
sudo cp /home/$USER/.config/monitors.xml to /var/lib/gdm3/.config/monitors.xml.

```

---

### Switch Identity

```
sudo dnf swap fedora-release-identity-workstation fedora-release-identity-server

```
