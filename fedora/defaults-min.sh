#!/bin/bash
sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm &&
sudo dnf install @base-x &&
sudo dnf install fish neovim lxpolkit rsync i3 i3blocks bspwm sxhkd rofi polybar alacritty akmod-nvidia picom pcmanfm NetworkManager-tui materia-gtk-theme papirus-icon-theme lxappearance util-linux-user xrandr pavucontrol pasystray nitrogen conky xsetroot dunst sxiv scrot galculator dmenu wine-core xarchiver freetype.i686 gnutls.i686 openldap.i686 libgpg-error.i686 sqlite2.i686 pulseaudio-libs.i686 vulkan-loader vulkan-loader.i686 lutris gamemode mpv cups neofetch htop libXft-devel libX11-devel libxkbcommon-devel
